#!/usr/bin/python3

import urllib.request
import sys

if len(sys.argv) == 1:
    urldata = input("Введи URL, с которого хочешь скачать файл - ")
    urllib.request.urlretrieve(urldata, 'favicon.ico' )
else:
    urllib.request.urlretrieve(sys.argv[1], 'favicon.ico' )
