FROM python:slim

WORKDIR /app/scripts

COPY faviconget.py ./ 

ENTRYPOINT ["python3","faviconget.py"]

#CMD ["https://google.com"] 

